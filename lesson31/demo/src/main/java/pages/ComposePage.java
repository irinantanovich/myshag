package pages;

import bo.Letter;
import org.openqa.selenium.By;

/**
 * Created by Main on 10.04.2017.
 */
public class ComposePage extends BasePage{

    private static final By RECEPIENT_INPUT_LOCATOR = By.xpath("//*[@name='to']/../input");
    private static final By SUBJECT_INPUT_LOCATOR = By.xpath("//*[@name='subj']");
    private static final By BODY_INPUT_LOCATOR = By.xpath("//*[@role='textbox']");
    private static final By SEND_BUTTON_LOCATOR = By.cssSelector(".js-send");

    public ComposePage sendLetter (Letter letter) {
        driver.findElement(RECEPIENT_INPUT_LOCATOR).sendKeys(letter.getRecipient());
        driver.findElement(SUBJECT_INPUT_LOCATOR).sendKeys(letter.getSubject());
        driver.findElement(BODY_INPUT_LOCATOR).sendKeys(letter.getBody());
        driver.findElement(SEND_BUTTON_LOCATOR).click();
        return this;
    }
}
