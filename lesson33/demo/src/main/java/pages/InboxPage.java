package pages;

import bo.Letter;
import org.openqa.selenium.By;

/**
 * Created by Account on 05.04.2017.
 */
public class InboxPage extends BasePage {

    private static final By LOGIN_NAME_LOCATOR = By.cssSelector(".mail-User-Name");
    private static final By NEW_LETTER_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    private static final String LETTER_LOCATOR_PATTERN = "//div[@class='mail-MessageSnippet-Content'][descendant::span[@title='%s']]";
    private static final By LETTER_BODY_LOCATOR = By.cssSelector("div.mail-Message-Body-Content>div");

    public boolean isLoginNameDisplayed() {
        return browser.isDisplayed(LOGIN_NAME_LOCATOR);
    }

    public ComposePage clickNewLetter() {
        browser.click(NEW_LETTER_BUTTON_LOCATOR);
        return new ComposePage();
    }

    public boolean isLetterPresent(Letter letter) {
        boolean isLetterDisplayed = browser.isDisplayed(By.xpath(String.format(LETTER_LOCATOR_PATTERN, letter.getSubject())));
        browser.click(By.xpath(String.format(LETTER_LOCATOR_PATTERN, letter.getSubject())));
        boolean isLetterContentPresent = browser.read(LETTER_BODY_LOCATOR).equals(letter.getBody());
        return  isLetterContentPresent && isLetterContentPresent;
    }

}
