package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;


public class YandexMailComposePage extends AbstractPage {

    private static final By FIELD_CAPTION_LOCATOR = By.xpath(".//div[@name='to']");
    private static final By SEND_BUTTON_LOCATOR = By.xpath("//button[@title='Отправить письмо (Ctrl + Enter)']");

    public boolean isFieldCaptionDisplayed() {
        return driver.findElement(FIELD_CAPTION_LOCATOR).isDisplayed();
    }

    public YandexMailComposePage inputAddressee(String addressee) {
        driver.findElement(FIELD_CAPTION_LOCATOR).sendKeys(addressee, Keys.ENTER);
        return this;
    }

    public YandexMailComposePage inputTextLetter(String textLetter) {
        driver.findElement(FIELD_CAPTION_LOCATOR).sendKeys(Keys.TAB, Keys.TAB, textLetter);
        return this;
    }

    public YandexMailDonePage clickSendButton() {
        driver.findElement(SEND_BUTTON_LOCATOR).click();
        return new YandexMailDonePage();
    }

}
