import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class SeleniumTestLocal {
    private static WebDriver driver;

    public static void main(String[] args) throws MalformedURLException {

        System.setProperty("webdriver.chrome.driver", new File("src\\main\\resources\\chromedriver.exe").getAbsolutePath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();

        driver.get("http://www.google.by/");

        waitForElementPresent(By.id("lst-ib"));

        WebElement searchInput = driver.findElement(By.id("lst-ib"));
        searchInput.sendKeys("banana song");

        //waitForElementPresent(By.name("btnK"));// кнопка Поиск в Google

        //driver.findElement(By.name("name=btnK")).click();

        waitForElementPresent(By.id("_fZ1"));// кнопка Поиск в Google
        driver.findElement(By.id("_fZ1")).click();


        waitForElementPresent(By.partialLinkText("Dispecable Me 2"));
        driver.findElement(By.partialLinkText("Dispecable Me 2")).click();

        System.out.println("Page title is: " + driver.getTitle());

        waitForElementPresent(By.className("watch-view-count"));
        driver.findElement(By.className("watch-view-count"));
        // ?? Нет метода assertEquals()
        assert (driver.findElement(By.className("watch-view-count")) equals regexp:5\d(,| )\5{3}(,| )\d{3}.*);

        driver.close();


    }

    private static void waitForElementPresent(By locator) {
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(locator));
    }


}


