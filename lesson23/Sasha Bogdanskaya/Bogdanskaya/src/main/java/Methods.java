import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Vel2 on 22.03.2017.
 */
public class Methods {

    public static File inputDir = new File("src\\main\\resources\\tables");
    public static List<File> fileList = (List<File>) FileUtils.listFiles(inputDir, null, true);
    public static String[] words;

    public static void addColumn(int position, String name, String... values) throws IOException {

        for (File file : fileList) {
            String fileContent = FileUtils.readFileToString(file);
            words = fileContent.split("[\\s\\r\\n|-]{2,1000}");
            List list = Arrays.asList(words);
            ArrayList<String> arrayWords = new ArrayList<String>(list);
            arrayWords.remove(0);

            int countColumn = arrayWords.size() / 4;
            if (values.length > 3) {
                System.out.println("Количество значений столбца превышает количество строк!");
                System.out.println();
            }
            if (position <= countColumn) {
                arrayWords.add(position - 1, name);
                for (int i = 0; i < 3; i++) {
                    arrayWords.add(position + countColumn*(i+1) + i, values[i]);
                }
            } else {
                System.out.println("Номер позиции столбца превышает количество всех столбцов!");
                System.out.println("Столбец будет добавлен в конец таблицы!");
                System.out.println();
                arrayWords.add(countColumn, name);
                for (int i = 0; i < 3; i++) {
                    arrayWords.add(countColumn*(i+2) + i+1, values[i]);
                }
            }

            printTable(arrayWords);
            System.out.println();
        }
    }

    public static void removeColumn(String name) throws IOException {

        for (File file : fileList) {
            String fileContent = FileUtils.readFileToString(file);
            words = fileContent.split("[\\s\\r\\n|-]{2,1000}");
            List list = Arrays.asList(words);
            ArrayList<String> arrayWords = new ArrayList<String>(list);
            arrayWords.remove(0);

            int countColumn = arrayWords.size() / 4;
            int index = arrayWords.indexOf(name);
            if (index >= 0) {
                for (int i = 3; i >= 0; i--) {
                    arrayWords.remove(index + countColumn*i);
                }
            } else {
                System.out.println("Столбец с указанным именем отсутствует!");
                System.out.println();
            }

            printTable(arrayWords);
            System.out.println();
        }
    }

    public static void editColumn(String name, String... values) throws IOException {

        for (File file : fileList) {
            String fileContent = FileUtils.readFileToString(file);
            words = fileContent.split("[\\s\\r\\n|-]{2,1000}");
            List list = Arrays.asList(words);
            ArrayList<String> arrayWords = new ArrayList<String>(list);
            arrayWords.remove(0);

            int countColumn = arrayWords.size() / 4;
            int index = arrayWords.indexOf(name);
            if (values.length > 3) {
                System.out.println("Количество значений столбца превышает количество строк!");
                System.out.println();
            }
            if (index >= 0) {
                for (int i = 0; i < values.length; i++) {
                    arrayWords.set(index + countColumn*(i+1), values[i]);
                }
            } else {
                System.out.println("Столбец с указанным именем отсутствует!");
                System.out.println();
            }

            printTable(arrayWords);
            System.out.println();
        }
    }

    public static void printTable(ArrayList<String> array) {

        int countColumn = array.size() / 4;
        int k = 0;
        String[][] table = new String[4][countColumn];

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < countColumn; j++) {
                table[i][j] = array.get(k);
                k++;
            }
        }

        int max = table[0][0].length();
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < countColumn; j++) {
                if (table[i][j].length() > max) {
                    max = table[i][j].length();
                }
            }
        }

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < countColumn; j++) {
                System.out.print("| " + table[i][j]);
                for (int l = 0; l < (max - table[i][j].length() + 1); l++) {
                    System.out.print(" ");
                }
            }
            System.out.println("|");
            for (int j = 0; j < max*countColumn+3*countColumn+1; j++) {
                System.out.print("-");
            }
            System.out.println();
        }
    }

}
