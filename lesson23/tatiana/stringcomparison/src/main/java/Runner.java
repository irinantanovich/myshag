import java.util.Arrays;
import java.util.Scanner;

public class Runner {
     public static void main(String[] args) {

         Scanner sc = new Scanner(System.in);
         String s1, s2;
         System.out.println("Введите первое слово: ");
         s1 = sc.nextLine();
         System.out.println("Введите второе слово: ");
         s2 = sc.nextLine();

         char[] chs1 = s1.toCharArray();
         char[] chs2 = s2.toCharArray();
         Arrays.sort(chs1);
         Arrays.sort(chs2);

         s1 = new String(chs1);
         s2 = new String(chs2);

         if (s1.equals(s2)) {
             System.out.println("Являются анаграммами");
         } else {
             System.out.println("Это не анаграммы");
         }
     }
 }