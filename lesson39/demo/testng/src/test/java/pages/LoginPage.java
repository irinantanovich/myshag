package pages;


import bo.Account;
import org.openqa.selenium.By;

public class LoginPage extends AbstractPage {

    private static final By LOGIN_BUTTON_LOCATOR = By.xpath("//input[@name='_login']");
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@id='password']");

    public LoginPage loginByAccount(Account account) {
        browser.type(LOGIN_INPUT_LOCATOR, account.getLogin());
        browser.type(PASSWORD_INPUT_LOCATOR, account.getPassword());
        return this;
    }

    public BuyTicketPage openBuyTicketPage() {
        browser.click(LOGIN_BUTTON_LOCATOR);
        return new BuyTicketPage();
    }
}
