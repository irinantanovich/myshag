package builder;

import bo.TrainRoute;

/**
 * Created by Vel2 on 20.04.2017.
 */
public class TrainRouteBuilder {

    public static TrainRoute createTrainRoute() {
        String stationDeparture = "ГОМЕЛЬ-ПАССАЖИРСКИЙ";
        String stationArrival = "МИНСК-ПАССАЖИРСКИЙ";
        return new TrainRoute(stationDeparture, stationArrival);
    }
}
