package reporting;


public class Logger {

    public final static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void debug(String s) {
        logger.debug(s);
    }

    public static void error(String s) {
        logger.error(s);
    }

    public static void info(String s) {
        logger.info(s);
    }

    public static void warn(String s) {
        logger.warn(s);
    }
}
